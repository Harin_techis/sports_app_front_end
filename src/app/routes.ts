import { Route } from "@angular/router";
import { AuthComponent } from "./comps/auth/auth.component";
import { DashboardComponent } from "./comps/dashboard/dashboard.component";
import { HomeComponent } from "./comps/home/home.component";
import { NewsListComponent } from "./comps/news-list/news-list.component";
import { SearchComponent } from "./comps/search/search.component";
import { AuthGuardService } from "./services/auth-guard.service";

export const appRoutes: Array<Route> = [
  { path: 'login', component: AuthComponent },
  {
    path: '', component: HomeComponent, canActivate: [AuthGuardService], children: [
      { path: '', component: DashboardComponent },
      { path: 'search/:search_for/:search_key', component: SearchComponent }
    ]
  }
];
