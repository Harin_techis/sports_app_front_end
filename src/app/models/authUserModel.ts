export interface authUser {
  email: string,
  fName: string,
  lName: string,
  fav_team_id: string,
  fav_team_name: string
}
