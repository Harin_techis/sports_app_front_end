import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

import { AppComponent } from './app.component';
import { AuthComponent } from './comps/auth/auth.component';
import { HomeComponent } from './comps/home/home.component';
import { HeadComponent } from './comps/head/head.component';
import { SearchComponent } from './comps/search/search.component';
import { NewsListComponent } from './comps/news-list/news-list.component';
import { DashboardComponent } from './comps/dashboard/dashboard.component';
import { SwiperSliderComponent } from './comps/swiper-slider/swiper-slider.component';
import { SearchResultCardComponent } from './comps/search-result-card/search-result-card.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HomeComponent,
    HeadComponent,
    SearchComponent,
    NewsListComponent,
    DashboardComponent,
    SwiperSliderComponent,
    SearchResultCardComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, HttpClientModule, NgxUsefulSwiperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
