import { Component, OnInit } from '@angular/core';
import { authUser } from 'src/app/models/authUserModel';
import { AuthService } from 'src/app/services/auth.service';
import { UserInfoService } from 'src/app/services/user-info-service.service';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css']
})
export class HeadComponent implements OnInit {
  authUser: authUser = null;
  menuOpen: boolean = false;
  constructor(
    private _userInfo: UserInfoService,
    private _authService: AuthService
  ) {
    this._userInfo.onAuthUserUpdate.subscribe(userInfo => this.authUser = userInfo);
  }

  ngOnInit(): void {
    this.authUser = this._userInfo.getAuthUser();
  }

  logout() {
    this._authService.logout();
  }

  toggle() {
    this.menuOpen = !this.menuOpen;
  }

}
