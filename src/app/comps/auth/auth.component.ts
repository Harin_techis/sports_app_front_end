import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DataFetchService } from 'src/app/services/data-fetch.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  defAuth = { email: "testuser@email.com", password: '123Qwe' };
  constructor(
    private _authService: AuthService,
    private _data_layer: DataFetchService
  ) { }

  ngOnInit(): void {
    this._data_layer.make_request('check_server', 'get').subscribe((response: any) => {
      console.log("Server status", response.data);
    }, error => {
      if (error && error.status !== 200) {
        console.log(error);
        window.alert('Server not responding. Please run backend server')
      };
    })
  }

  onSubmit(formValue, formState) {
    // console.log(formValue, formState);
    if (formState) this._authService.login(formValue);
  }

}
