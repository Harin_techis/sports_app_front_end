import { Component, Input, OnInit } from '@angular/core';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-swiper-slider',
  templateUrl: './swiper-slider.component.html',
  styleUrls: ['./swiper-slider.component.css']
})
export class SwiperSliderComponent implements OnInit {
  config: SwiperOptions = {
    pagination: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30,
    slidesPerView: 3
  };
  @Input('data-set') data_set: Array<any>;
  @Input('data-model') data_model: Array<any>;
  constructor() { }

  ngOnInit(): void {
  }

}
