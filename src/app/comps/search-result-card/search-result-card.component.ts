import { Input, Component, OnInit } from '@angular/core';
import { authUser } from 'src/app/models/authUserModel';
import { UserInfoService } from 'src/app/services/user-info-service.service';

@Component({
  selector: 'app-search-result-card',
  templateUrl: './search-result-card.component.html',
  styleUrls: ['./search-result-card.component.css']
})
export class SearchResultCardComponent implements OnInit {
  @Input('data-set') data_set: any;
  @Input('data-model') data_model: 'team' | 'player' | 'event';
  @Input('data-fav-team') data_fav_team: string;

  authUser: authUser = null;
  constructor(
    private _user_info_service: UserInfoService
  ) { }

  ngOnInit(): void {

  }

  select_fav_team(teamId, teamName?: string) {
    console.log(teamId);
    this._user_info_service.select_fav_team(teamId, teamName);
  }

}
