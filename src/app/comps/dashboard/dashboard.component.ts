import { Component, OnInit } from '@angular/core';
import { authUser } from 'src/app/models/authUserModel';
import { DataFetchService } from 'src/app/services/data-fetch.service';
import { UserInfoService } from 'src/app/services/user-info-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  authUser: authUser = null;
  search_results: { key: string, teams?: Array<any>, players?: Array<any>, events?: Array<any> } = { key: null, teams: null, players: null, events: null };
  recentEvents = null;
  fav_team_name: string = null;
  // fav_team_id: string = null;
  response = null;
  search_response;
  constructor(
    private _data_layer: DataFetchService,
    private _userInfoServ: UserInfoService
  ) {
    this._userInfoServ.onAuthUserUpdate.subscribe(userInfo => {
      this.authUser = userInfo;
      this.fetchRecentEvents();
    });
  }

  ngOnInit(): void {
    // console.log(window.localStorage.getItem('fav_team_id'));
    this.authUser = this._userInfoServ.getAuthUser();
    // this.fav_team_id = window.localStorage.getItem('fav_team_id') || this.authUser ? this.authUser.fav_team_id : null;
    this.fetchRecentEvents();
  }

  fetchRecentEvents() {
    // console.log(this.authUser);
    if (this.authUser && this.authUser.fav_team_id) {
      this._data_layer.make_request('api/last_5_events?team_id=' + this.authUser.fav_team_id, 'get').subscribe(
        (response: any) => {
          this.recentEvents = response.data;
          // console.log(this.recentEvents);
          this.fav_team_name = this.recentEvents.results ? this.recentEvents.results[0]["strHomeTeam"] : null;
          // // this.search_results.
          // // console.log(JSON.parse(response.data));
        }, error => {
          window.alert("Something went wrong in fetching recent events")
          console.log(error);
        }
      )
    }
  }

  search(search_key: string) {
    this.search_results.key = search_key;
    this._data_layer.make_request('api/search?search=' + search_key, 'get').subscribe(
      (response: any) => {
        // console.table(response.data);
        this.search_results = response.data //.teams ? response.data.teams : null;
        this.search_results["key"] = search_key;
        // this.search_response.players = response.data.players ? response.data.players : null;
        // console.log(this.search_results);
      }, error => {
        window.alert("Something went wrong in searching");
        console.log(error);
      }
    )
  }

}
