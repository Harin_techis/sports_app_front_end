import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DataFetchService } from './data-fetch.service';
import { UserInfoService } from './user-info-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authenticated = false;
  constructor(
    private _router: Router,
    private data_layer: DataFetchService,
    private _userInfoService: UserInfoService
  ) {
  }

  isAuthenticated() {
    return this.authenticated;
  }

  login(auth_data: { email: string, password: string }) {
    this.data_layer.make_request('auth/login', 'post', auth_data)
      .subscribe((response: any) => {
        // console.log(response);
        this._userInfoService.setAuthUser(response.data ? response.data : null);
        this.authenticated = true;
        this._router.navigate(['/']);
      }, error => {
        window.alert(error.error.message);
      });
  }

  logout() {
    window.localStorage.removeItem("user_info");
    this._userInfoService.setAuthUser(null);
    this.authenticated = false;
    this._router.navigate(['/login']);

  }
}
