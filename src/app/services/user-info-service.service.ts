import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { authUser } from '../models/authUserModel';

@Injectable({
  providedIn: 'root'
})

export class UserInfoService {
  auth_user: authUser = null;
  onAuthUserUpdate = new Subject<authUser>();
  constructor() { }

  setAuthUser(userData?: any) {
    this.auth_user = userData ? userData : null;
    const existing_fav_team_id = window.localStorage.getItem('fav_team_id');
    const existing_fav_team_name = window.localStorage.getItem('fav_team_name');
    this.auth_user.fav_team_id = existing_fav_team_id && existing_fav_team_id !== "" && this.auth_user ? existing_fav_team_id : null;
    this.auth_user.fav_team_name = existing_fav_team_name && existing_fav_team_name !== "" && this.auth_user ? existing_fav_team_name : null;
    this.onAuthUserUpdate.next(this.auth_user);
  }

  getAuthUser() {
    return this.auth_user;
  }

  select_fav_team(team_id: string, teamName?: string) {
    this.auth_user.fav_team_id = team_id;
    this.auth_user.fav_team_name = teamName;
    window.localStorage.setItem('fav_team_id', team_id);
    window.localStorage.setItem('fav_team_name', teamName);
    this.onAuthUserUpdate.next(this.auth_user);
    if (teamName) window.alert(teamName + " is selected as your favourite team");
  }
}
