import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataFetchService {
  base_url = environment.api_base_url;
  constructor(private _http: HttpClient) { }

  make_request(path: string, method?: string, post_object?: any) {
    if (!method || method.toLocaleLowerCase() == "get") {
      return this._http.get(this.base_url + path);
    }
    if (method.toLocaleLowerCase() == 'post') {
      return this._http.post(this.base_url + path, post_object);
    }
  }
}
